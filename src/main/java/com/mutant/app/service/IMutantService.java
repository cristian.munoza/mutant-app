package com.mutant.app.service;


import com.mutant.app.entity.MutantEntity;
import com.mutant.app.model.request.DnaRequestDTO;
import com.mutant.app.model.response.StatsResponseDTO;

import java.util.Optional;

/**
 * Mutant Service
 */
public interface IMutantService {

    /**
     * Validates DNA sequence to determine wether the subject is a mutant or human, and saves the DNA sequence into
     * the database
     *
     * @param request the DNA sequence to validate and add into the database
     * @return true if the inserted sequence corresponds to mutant dna, false otherwise
     */
    boolean validateDNA(DnaRequestDTO request);
    StatsResponseDTO getMutantStats();
    Optional<MutantEntity> isMutantInDB(String id);

}
