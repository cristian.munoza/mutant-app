package com.mutant.app.util;

public class Constants {

    public static final String BASE_PACKAGES_REPOSITORY = "com.mutant.app.repository";
    public static final String PATH_CONTROLLER  = "/validate";
    public static final String PATH_VALIDATE_MUTANT = "/mutant";
    public static final String PATH_GET_STATS = "/stats";

    public static final String REGION_AWS = "us-east-1";
    public static final String ENDPOINT_DYNAMO = "dynamodb.us-east-1.amazonaws.com";

    public static final String GENERAL_ERROR_CODE = "ER-00";

}
