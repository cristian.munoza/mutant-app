package com.mutant.app.config;

import com.mutant.app.logger.ILoggerStrategy;
import com.mutant.app.logger.Log4jImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoggerConfig {

    @Bean
    @ConditionalOnProperty(name = "logger.impl",havingValue = "log4j")
    public ILoggerStrategy LogStrategy (){
        return new Log4jImpl();
    }
}
