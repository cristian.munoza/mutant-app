package com.mutant.app.exception;

import com.mutant.app.model.error.Error;
import com.mutant.app.util.Constants;
import com.mutant.app.util.ResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpServerErrorException;

@ControllerAdvice
public class MutantExceptionHandler {

    @ExceptionHandler(HttpServerErrorException.InternalServerError.class)
    public ResponseEntity generalError(Exception e) {
        Error error = new Error(Constants.GENERAL_ERROR_CODE,e.getMessage());
        return ResponseUtil.error(error);
    }
}
