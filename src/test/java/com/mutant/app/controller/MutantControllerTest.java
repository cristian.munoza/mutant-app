package com.mutant.app.controller;

import com.mutant.app.logger.ILoggerStrategy;
import com.mutant.app.model.request.DnaRequestDTO;
import com.mutant.app.model.response.StatsResponseDTO;
import com.mutant.app.service.MutantService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class MutantControllerTest {

    private MutantController mutantController;
    @Mock
    private MutantService mutantService;

    @Mock
    private ILoggerStrategy loggerStrategy;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mutantController = new MutantController(loggerStrategy, mutantService);
    }

    @Test
    public void testValidateDna_mutant() {
        DnaRequestDTO dnaRequestDTO = buildDnaRequest();
        given(mutantService.validateDNA(dnaRequestDTO)).willReturn(true);

        ResponseEntity response = mutantController.validateDna(dnaRequestDTO);

        verify(mutantService,times(1)).validateDNA(dnaRequestDTO);
        assertEquals(response.getStatusCode(),HttpStatus.OK);
    }

    @Test
    public void testValidateDna_human() {
        DnaRequestDTO dnaRequestDTO = buildDnaRequest();
        given(mutantService.validateDNA(dnaRequestDTO)).willReturn(false);

        ResponseEntity response = mutantController.validateDna(dnaRequestDTO);

        verify(mutantService,times(1)).validateDNA(dnaRequestDTO);
        assertEquals(response.getStatusCode(),HttpStatus.FORBIDDEN);
    }

    @Test
    public void testGetDnaStats() {
        StatsResponseDTO statsResponseDTO = buildStatsResponse();
        given(mutantService.getMutantStats()).willReturn(statsResponseDTO);

        ResponseEntity response = mutantController.getDnaStats();

        verify(mutantService,times(1)).getMutantStats();

        assertEquals(response.getStatusCode(),HttpStatus.OK);
        assertEquals(((StatsResponseDTO) response.getBody()).getCountHumanDna(),10);
        assertEquals(((StatsResponseDTO) response.getBody()).getCountMutantDna(),15);
        assertEquals(((StatsResponseDTO) response.getBody()).getRatio(),1.5);
    }

    private DnaRequestDTO buildDnaRequest() {
        String[] dna = {"CTCTGT", "CACAAT", "CGAAAA", "AGCGCG", "TATTGA", "AGGGGC"};
        DnaRequestDTO dnaRequestDTO = new DnaRequestDTO();
        dnaRequestDTO.setDna(dna);
        return dnaRequestDTO;
    }

    private StatsResponseDTO buildStatsResponse() {
        StatsResponseDTO statsResponseDTO = new StatsResponseDTO(15,10,1.5);
        return statsResponseDTO;
    }
}
