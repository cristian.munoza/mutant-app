package com.mutant.app.service;

import com.mutant.app.entity.MutantEntity;
import com.mutant.app.logger.ILoggerStrategy;
import com.mutant.app.model.request.DnaRequestDTO;
import com.mutant.app.repository.IMutantRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.mockito.Mockito.*;

public class MutantServiceTest {

    private MutantService mutantService;

    @Mock
    private  MutantValidator mutantValidator;

    @Mock
    private IMutantRepository mutantRepository;

    @Mock
    private ILoggerStrategy logStrategy;

    @BeforeEach
    public void setUp(){

        MockitoAnnotations.openMocks(this);
        mutantService = Mockito.spy(new MutantService(mutantValidator,mutantRepository,logStrategy));


    }

    @Test
    public void testValidateDNANoDB() {

        //Given
        String[] dna = new String[]{"ATTGCC","ATCGCC","ATCGCC","ATCGCC","ATCGCC","ATCGCC"};
        Optional<MutantEntity> mutant = Optional.empty() ;

        //When
        doReturn(mutant).when(mutantService).isMutantInDB("d4490fa59eb1608aa244b7295224aca0");
        when(mutantValidator.isMutant(dna)).thenReturn(true);
        boolean response = mutantService.validateDNA(new DnaRequestDTO(dna));

        //Then
        verify(mutantValidator,times(1)).isMutant(dna);
        verify(mutantRepository,times(1)).save(ArgumentMatchers.any(MutantEntity.class));
        Assertions.assertNotNull(response);
        Assertions.assertEquals(response,true);


    }

    @Test
    public void testValidateDNADB() {

        //Given
        String[] dna = new String[]{"ATTGCC","ATCGCC","ATCGCC","ATCGCC","ATCGCC","ATCGCC"};
        Optional<MutantEntity> mutant = Optional.of(new MutantEntity("d4490fa59eb1608aa244b7295224aca0","ATTGCC|ATCGCC|ATCGCC|ATCGCC|ATCGCC|ATCGCC",false)) ;

        //When
        doReturn(mutant).when(mutantService).isMutantInDB("d4490fa59eb1608aa244b7295224aca0");
        boolean response = mutantService.validateDNA(new DnaRequestDTO(dna));

        //Then
        verify(mutantService,times(1)).isMutantInDB("d4490fa59eb1608aa244b7295224aca0");
        Assertions.assertNotNull(response);
        Assertions.assertEquals(response,false);
    }


}
