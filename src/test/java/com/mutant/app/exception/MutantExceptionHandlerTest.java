package com.mutant.app.exception;

import com.mutant.app.model.error.Error;
import com.mutant.app.util.Constants;
import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class MutantExceptionHandlerTest {

    private MutantExceptionHandler mutantExceptionHandler;

    @BeforeEach
    public void setUp(){
        mutantExceptionHandler = new MutantExceptionHandler();
    }

    @Test
    public void testGeneralError(){
        Exception exception = new Exception("Internal Server Error");
        ResponseEntity<Error> errorResponseEntity = mutantExceptionHandler.generalError(exception);

        assertNotNull(errorResponseEntity);
        assertEquals(errorResponseEntity.getStatusCode(),HttpStatus.INTERNAL_SERVER_ERROR);
        assertEquals(((Error) errorResponseEntity.getBody()).getCodError(),Constants.GENERAL_ERROR_CODE);
        assertEquals(((Error) errorResponseEntity.getBody()).getMsgError(),"Internal Server Error");
    }
}
